<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UsersFriends;
use Validator;
use Response;

class FriendsController extends Controller
{

  public function sendFriendRequest(Request $req){
    $validator = Validator::make($req->all(),[
      'user_id'=>'required',
      'friend_user_id'=>'required'
    ]);
    if($validator->fails()){
      return Response::json($validator->messages(),410);
    }else{
      $checkUserFriendShip = UsersFriends::where('user_id',$req->user_id)->where('friend_user_id',$req->friend_user_id)->first();
      if($checkUserFriendShip == null){
        $addFriend = new UsersFriends();
        $addFriend->user_id = $req->user_id;
        $addFriend->friend_user_id  = $req->friend_user_id;
        $addFriend->save();
        return Response::json(['status'=>200,'message'=>'request sent successfully']);
      }else{
        if($checkUserFriendShip->request_status == 0){

          return Response::json(['status'=>500,'message'=>'Friend Request Sent Already']);

        }else if($checkUserFriendShip->request_status == 2){
          return Response::json(['status'=>500,'message'=>'user blocked you for adding as friend']);

        }else{
          return Response::json(['status'=>500,'message'=>'user already added to friend list']);
        }
      }
    }
  }


  public function requestAction(Request $req){
    $validator = Validator::make($req->all(),[
      'friend_user_id'=>'required',
      'user_id'=>'required',
      'request_status'=>'required'
    ]);
    if($validator->fails()){
      return Response::json($validator->messages(),410);
    }else{
      $checkUserFriendShip = UsersFriends::where('user_id',$req->user_id)->where('friend_user_id',$req->friend_user_id)->first();
      // dd($checkUserFriendShip);
      if($checkUserFriendShip == null){
        return Response::json(['status'=>500,'message'=>'something went wrong']);
      }else{
        $checkUserFriendShip->request_status = $req->request_status;
        $checkUserFriendShip->update();
        if($req->request_status == 1){
          $checkCon = UsersFriends::where('user_id',$req->friend_user_id)->first();
          if($checkCon == null){
            $addContact = new UsersFriends();
            $addContact->user_id = $req->friend_user_id;
            $addContact->friend_user_id = $req->user_id;
            $addContact->request_status = 1;
            $addContact->save();
          }else{
            $checkCon->request_status = 1;
            $checkCon->update();
          }

          return Response::json(['status'=>200,'message'=>'contact added successfully']);
        }else{
          return Response::json(['status'=>200,'message'=>'Contact Declined']);
        }
      }
    }
  }

}
