<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UsersFriends;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sentRequests = Array();
      $pendingRequests = Array();
      $Friends = Array();
      $count1 = 0;
      $count2 = 0;
      $count3 = 0;

        $getSentRequests = UsersFriends::where('user_id',Auth::user()->id)->where('request_status',0)->get();
        if(count($getSentRequests) != 0){

          foreach($getSentRequests as $gsr){
            $getUser = User::where('id',$gsr->friend_user_id)->select('name','email')->first();
            $sentRequests[$count1] = $getUser;
            $count1++;
          }
        }
        // dd($sentRequests);
        $getPendingRequests = UsersFriends::where('friend_user_id',Auth::user()->id)->where('request_status',0)->get();
        // dd($getPendingRequests);
        if(count($getPendingRequests) != 0){
          foreach($getPendingRequests as $gsr){
            $getUser = User::where('id',$gsr->user_id)->select('name','email','id')->first();
            $pendingRequests[$count2] = $getUser;
            $count2++;
          }
        }
        $getFriends = UsersFriends::where('user_id',Auth::user()->id)->where('request_status',1)->get();
        // dd($getFriends);
        if(count($getFriends)!= 0){
          foreach($getFriends as $gsr){
            // dd($gsr);
            $getUser = User::where('id',$gsr->friend_user_id)->select('name','email','id')->first();
            $Friends[$count3] = $getUser;
            $count3++;
          }
        }
        // dd($Friends,Auth::user()->id);

        return view('home',compact('sentRequests','pendingRequests','Friends'));
    }

    public function fetchData(Request $req){
      $query = $req->get('query');
      // dd($query);
      // $user = User::where('name','test1')->first();
      $data = User::where('name','like','%'.$query.'%')->get();
      $output = '<ul class="dropdown-menu" style="display:block; position:relative;">';
      // dd($data);
      foreach($data as $d){
        $output .= '<li style="margin-left:5px"><a href="/profile/'.$d->id.'">'.$d->name.'</a></li>';
      }
      $output .= '</ul>';
      // dd($output);
      return $output;
    }

    public function profile($id){
      $user = User::where('id',$id)->first();
      $checkStatus = UsersFriends::where('user_id',Auth::user()->id)->where('friend_user_id',$id)->first();
      // dd($checkStatus);
      $getUsersFriends = UsersFriends::where('user_id',Auth::user()->id)->pluck('friend_user_id');
      $getMutualFriends = UsersFriends::where('user_id',$id)->whereIn('friend_user_id',$getUsersFriends)->pluck('friend_user_id');
      $getMutualUsers = User::whereIn('id',$getMutualFriends)->select('name','email')->get();
      $getMutualUsersCount = count($getMutualUsers);
      // dd($getUsers[0]['name']);
      return view('profile',compact('user','checkStatus','getMutualUsers','getMutualUsersCount'));
    }
}
