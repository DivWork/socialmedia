<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UserTableSeeder');

        $this->command->info('User table seeded!');
    }
}
class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(['email' => 'test1@test.com','name'=>'test1','password'=>bcrypt('password')]);
        User::create(['email' => 'test2@test.com','name'=>'test2','password'=>bcrypt('password')]);
        User::create(['email' => 'test3@test.com','name'=>'test3','password'=>bcrypt('password')]);
        User::create(['email' => 'test4@test.com','name'=>'test4','password'=>bcrypt('password')]);
        User::create(['email' => 'test5@test.com','name'=>'test5','password'=>bcrypt('password')]);
        User::create(['email' => 'test6@test.com','name'=>'test6','password'=>bcrypt('password')]);
        User::create(['email' => 'test7@test.com','name'=>'test7','password'=>bcrypt('password')]);

    }

}
