@extends('layouts.app')

@section('content')
<link media="all" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2><center>Welcome {{ Auth::user()->name }}</center></h2></div>

                <div class="panel-body">
                    <!-- @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif -->
                       <ul class="nav nav-tabs nav-justified">
                         <li class="active"><a data-toggle="tab"  href="#searchFri">Search Users</a></li>
                         <li><a data-toggle="tab"  href="#FriendList">Friends List</a></li>
                         <li><a data-toggle="tab"  href="#FriendRequest">Pending Requests</a></li>
                         <li><a data-toggle="tab"  href="#FriendRequestSent">Friend Requests Sent</a></li>
                        </ul>
                       <br>
                       <!-- <p><strong>Note:</strong> This example shows how to create a basic navigation tab. It is not toggleable/dynamic yet (you can't click on the links to display different content)- see the last example in the Bootstrap Tabs and Pills Tutorial to find out how this can be done.</p> -->
                      <!-- </div> -->
                      <div class="tab-content">
                        <div id="searchFri" class="row tab-pane fade in active">
                          <div class="col-sm-6 col-sm-offset-3">
                              <div class="form-group">
                                  <div class="input-group stylish-input-group">
                                      <input id="searchContent" type="text" class="form-control"  placeholder="Search" >
                                      <span class="input-group-addon">
                                          <button id="testSearch">
                                              <span class="fa fa-search"></span>
                                          </button>
                                      </span>
                                  </div>
                                  <div id="searchList"></div>
                              </div>
                              {{ csrf_field() }}
                          </div>
                  	</div>

                    <div id="FriendList" class="row tab-pane fade">
                      <div class="col-sm-6 col-sm-offset-5">
                        <h1 style="color:red"> Friends List</h1>
                        <br/>
                      </div>
                      <div class="col-sm-6 col-sm-offset-3">
                      <table class="table table-hover">
                          <thead>
                            <tr>
                              <th><center>Friend's Name</center></th>
                              <th><center>Friend's Email</center></th>
                             </tr>
                          </thead>
                          <tbody>
                            @foreach($Friends as $fr)
                            <tr>
                              <td><center><a href="profile/{{$fr['id']}}">{{$fr['name']}}</center></a></td>
                              <td><center>{{$fr['email']}}</center></td>
                            </tr>
                            @endforeach
                          </tbody>
                      </table>
                    </div>
                    </div>

                    <div id="FriendRequest" class="row tab-pane fade">
                      <div class="col-sm-6 col-sm-offset-5">
                        <h1 style="color:#900C3F"> Pending Requests</h1>
                        <br/>
                      </div>
                      <div class="col-sm-6 col-sm-offset-3">
                      <table class="table table-hover">
                          <thead>
                            <tr>
                              <th><center>Friend's Name</center></th>
                              <th><center>Friend's Email</center></th>
                              <th><center>Action</center></th>
                             </tr>
                          </thead>
                          <tbody>
                            @foreach($pendingRequests as $fr)
                            <tr>
                              <td><center>{{$fr['name']}}</center></td>
                              <td><center>{{$fr['email']}}</center></td>
                              <td><center><div class="btn-group" role="group">
                                  <button type="button" class="btn btn-primary" onclick="accept({{$fr['id']}});">accept</button>
                                  <button type="button" class="btn btn-secondary" onclick="decline({{$fr['id']}});">decline</button>
                                </div></center>
                              </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                    </div>

                    </div>

                    <div id="FriendRequestSent" class="row tab-pane fade">
                      <div class="col-sm-6 col-sm-offset-5">
                        <h1 style="color:orange"> Sent Requests</h1>
                        <br/>
                      </div>
                      <div class="col-sm-6 col-sm-offset-3">
                      <table class="table table-hover">
                          <thead>
                            <tr>
                              <th><center>Friend's Name</center></th>
                              <th><center>Friend's Email</center></th>
                              <th><center>Action</center></th>
                             </tr>
                          </thead>
                          <tbody>
                            @foreach($sentRequests as $fr)
                            <tr>
                              <td><center>{{$fr['name']}}</center></td>
                              <td><center>{{$fr['email']}}</center></td>
                              <td><center><div class="btn">
                                  <button type="button" class="btn btn-warning">Sent</button>
                                </div></center>
                              </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                    </div>

                    </div>
                </div>

                  </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

function accept(id){
  // console.log(id);
  $.ajax({
    url:"/api/requestAction",
    method:"POST",
    data:{friend_user_id:'<?= Auth::user()->id ?>',user_id:id,request_status:1},
    success:function(data){
      console.log(data);
      if(data.status == 200){
        alert(data.message);
        location.href = '';
      }

    },
    error:function(err){
      alert(err);
    }
  });
}

function decline(id){
  console.log(id);
  $.ajax({
    url:"/api/requestAction",
    method:"POST",
    data:{friend_user_id:'<?= Auth::user()->id ?>',user_id:id,request_status:2},
    success:function(data){
      if(data.status == 200){
        alert(data.message);
        location.href = '';
      }
    },
    error:function(err){
      alert(err);
    }
  });
}

$(document).ready(function(){
  $('#searchContent').keyup(function(){
    var searchContent = $(this).val();
    if(searchContent != ''){
      // console.log(searchContent);
      var _token = $('input[name="_token"]').val();
      console.log(_token);
      $.ajax({
        url:"{{ route('searchComplete.fetch') }}",
        method:"POST",
        data:{query:searchContent,_token:_token},
        success:function(data){
          $('#searchList').fadeIn();
          $('#searchList').html(data);

          console.log(data);
        },
        error:function(err){
          alert(err);
        }

      });

    }else{
      $('#searchList').empty();
    }
  });
});
</script>
@endsection
