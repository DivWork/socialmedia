@extends('layouts.app')

@section('content')

<link media="all" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2><center>{{$user->name}}'s Profile</center></h2></div>

                <div class="panel-body">
                  <div class="col-md-4 col-md-offset-1">
                    <h3> Details</h3>
                    <h5> User Name : {{$user->name}} </h5>
                     <h5> User Email : {{$user->email}} </h5>
                  </div>
                  <div class="col-md-4 col-md-offset-3">
                    <h3>Friendship Status</h3>
                    @if($checkStatus == null)
                    <div class="btn btn-primary" onclick="addFriend({{$user->id}});"> Add Friend </div>
                    @elseif($checkStatus->request_status == 0)
                    <div class="btn btn-warning"> Request Sent </div>
                    @elseif($checkStatus->request_status == 1)
                    <div class="btn btn-success"> Friends </div>
                    @endif


                  </div>

                </div>
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-default">
              <div class="panel-heading"><h2><center>Mutual Friends</center></h2></div>

              <div class="panel-body">
                <div class="col-md-4 col-md-offset-1">
                  <h5> Total Mutual Friends: {{$getMutualUsersCount}} People</h5>
                </div>
                <div class="col-md-8 col-md-offset-1">
                  <table class="table table-striped">
                      <thead>
                        <tr>
                          <th> Name </th>
                          <th> Email </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($getMutualUsers as $gm)
                          <tr>
                              <td>{{$gm->name}}</td>
                              <td>{{$gm->email}}</td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
                </div>
              </div>
        </div>
    </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

  function addFriend(id){
    // console.log(id);
    $.ajax({
      url:"/api/sendFriendRequest",
      method:"POST",
      data:{user_id:'<?= Auth::user()->id ?>',friend_user_id:id},
      success:function(data){
        if(data.status == 200){
          alert(data.message);
          location.href = '';
        }

      },
      error:function(err){
        alert(err);
      }
    });
  }
</script>


@endsection
